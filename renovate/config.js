module.exports = {
    extends: ['config:base'],
    enabledManagers: ['npm'],
    platform: 'gitlab',
    baseBranches: ['main'],
    repositories: ['lauritz.andreas/renovate-test'],
  };
  